const switchMatch = require('../src/index.js');

test('Works with predicate', ()=>{
	const predicate = jest.fn(()=>true);

	const [s, c] = switchMatch(predicate);

	const symbol = s('subject');

	expect(c('case')).toBe(symbol);

	expect(predicate).toHaveBeenCalledWith('subject', 'case');
})

test('Works with default predicate', ()=>{
	const subject = jest.fn(()=>true);

	const [s, c] = switchMatch();

	const symbol = s(subject);

	expect(c('case')).toBe(symbol);

	expect(subject).toHaveBeenCalledWith('case');
})


test('Doesn\'t match on falsy values', ()=>{
	const subject = jest.fn(()=>false);

	const [s, c] = switchMatch();

	s(subject);

	expect(c('case')).toBe(null);

	expect(subject).toHaveBeenCalledWith('case');
})

test('Case throws if called before subject', ()=>{
	const [s, c] = switchMatch();

	expect(c).toThrow();
})
