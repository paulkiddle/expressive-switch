function switchMatch(predicate = (s,v) => s(v)){
	let successSymbol;
	let expression;
	return [
		function setSubject(sub){
			expression = sub;
			return successSymbol = Symbol('Matches');
		},
		function testCase(value) {
			if(!expression) {
				throw new TypeError('No test expression has been set.');
			}
			return predicate(expression, value) ? successSymbol : null;
		}
	];
}

module.exports = switchMatch;
